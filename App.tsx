import React from 'react';
import { View } from 'react-native';
import ProfileNavigator from '../day8/src/app/ProfileNavigator';

const App = () => {
  return (
    <View>
      <ProfileNavigator />
    </View>
  );
};

export default App;
