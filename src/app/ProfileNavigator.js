import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ProfileInput from '../app/ProfileInput';
import {SafeAreaView} from 'react-native-safe-area-context';

const ProfileNavigator = () => {
  const [profile, setProfile] = useState(null);
  const [showInput, setShowInput] = useState(false);

  const getProfile = async () => {
    const jsonValue = await AsyncStorage.getItem('@profile');
    setProfile(jsonValue != null ? JSON.parse(jsonValue) : null);
  };

  const saveProfile = async newProfile => {
    await AsyncStorage.setItem('@profile', JSON.stringify(newProfile));
    setProfile(newProfile);
    setShowInput(false);
  };

  useEffect(() => {
    getProfile();
    console.log(getProfile());
  }, []);

  return (
    <SafeAreaView style={styles.css.container}>
      <View>
        {showInput ? (
          <ProfileInput onSave={saveProfile} />
        ) : (
          <View>
            {profile ? (
              <View>
                <Text style={styles.css.kolnama}>Name: {profile.name}</Text>
                <Text style={styles.css.kolumur}>Age: {profile.age}</Text>
                <Text style={styles.css.kolemail}>email: {profile.email}</Text>
                <Text style={styles.css.kolnohp}>No Hp: {profile.nohp}</Text>
                <Text style={styles.css.kolalamat}>
                  Alamat: {profile.alamat}
                </Text>
                <TouchableOpacity
                  style={styles.css.tomEdit}
                  onPress={() => setShowInput(true)}>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontSize: 15,
                      marginTop: 20,
                      color: 'white',
                      fontWeight: 'bold',
                    }}>
                    Edit Profile
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <TouchableOpacity
                style={styles.css.tomCreate}
                onPress={() => setShowInput(true)}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: 15,
                    marginTop: 20,
                    color: 'white',
                    fontWeight: 'bold',
                  }}>
                  Create Profile
                </Text>
              </TouchableOpacity>
            )}
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  css: {
    container: {
      backgroundColor: '#6b4ab7',
      marginTop: Platform.OS == 'android' ? 30 : 60,
      height: Platform.OS == 'android' ? 450 : 450,
      borderRadius: 20,
    },
    kolnama: {
      borderWidth: 0.5,
      overflow: 'hidden',
      borderColor: '#027588',
      borderRadius: 20,
      backgroundColor: 'white',
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    kolumur: {
      borderWidth: 0.5,
      borderColor: '#027588',
      overflow: 'hidden',
      backgroundColor: 'white',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    kolemail: {
      borderWidth: 0.5,
      borderColor: '#027588',
      overflow: 'hidden',
      backgroundColor: 'white',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    kolnohp: {
      borderWidth: 0.5,
      borderColor: '#027588',
      overflow: 'hidden',
      backgroundColor: 'white',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    kolalamat: {
      borderWidth: 0.5,
      borderColor: '#027588',
      backgroundColor: 'white',
      overflow: 'hidden',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    tomEdit: {
      textAlign: 'center',
      backgroundColor: '#1f303a',
      fontWeight: 'bold',
      alignItem: 'center',
      fontSize: 25,
      width: 200,
      height: 70,
      borderRadius: 45,
      marginTop: 30,
      marginLeft: 90,
    },
    tomCreate: {
      color: 'black',
      textAlign: 'center',
      backgroundColor: '#1f303a',
      fontWeight: 'bold',
      alignItem: 'center',
      fontSize: 25,
      width: 200,
      height: 70,
      borderRadius: 45,
      marginTop: 30,
      marginLeft: 90,
    },
  },
});

export default ProfileNavigator;
