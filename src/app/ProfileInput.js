import React, {useState} from 'react';
import {
  View,
  TextInput,
  SafeAreaView,
  StyleSheet,
  Platform,
  TouchableOpacity,
  Text,
} from 'react-native';
import {} from 'react-native-safe-area-context';

const ProfileInput = ({onSave}) => {
  const [name, setName] = useState('');
  const [age, setAge] = useState('');
  const [email, setEmail] = useState('');
  const [nohp, setNohp] = useState('');
  const [alamat, setAlamat] = useState('');

  const handleSave = () => {
    onSave({name, age, email, nohp, alamat});
    console.log('nama :' + name, age, email, nohp, alamat);
  };
  return (
    <SafeAreaView style={styles.canvas.container}>
      <Text
        style={{
          fontSize: 20,
          fontWeight: 'bold',
          textAlign: 'center',
        }}>
        Isi Biodata
      </Text>
      <View>
        <TextInput
          style={styles.canvas.cssnama}
          placeholder="Name"
          value={name}
          onChangeText={setName}
        />
        <TextInput
          style={styles.canvas.cssumur}
          placeholder="Age"
          value={age}
          onChangeText={setAge}
        />
        <TextInput
          style={styles.canvas.cssEmail}
          placeholder="Email"
          value={email}
          onChangeText={setEmail}
        />
        <TextInput
          style={styles.canvas.cssNohp}
          placeholder="No HP"
          value={nohp}
          onChangeText={setNohp}
        />
        <TextInput
          style={styles.canvas.cssAlamat}
          placeholder="Alamat"
          value={alamat}
          onChangeText={setAlamat}
        />
        <TouchableOpacity style={styles.canvas.cssTombol} onPress={handleSave}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 15,
              marginTop: 20,
              color: 'white',
              fontWeight: 'bold',
            }}>
            Save
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  canvas: {
    container: {
      backgroundColor: '#6b4ab7',
      marginTop: Platform.OS == 'android' ? 30 : 60,
      height: Platform.OS == 'android' ? 485 : 477,
      borderRadius: 20,
    },
    cssnama: {
      borderWidth: 0.5,
      borderColor: '#027588',
      backgroundColor: 'white',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    cssumur: {
      borderWidth: 0.5,
      borderColor: '#027588',
      backgroundColor: 'white',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    cssEmail: {
      borderWidth: 0.5,
      borderColor: '#027588',
      backgroundColor: 'white',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    cssNohp: {
      borderWidth: 0.5,
      borderColor: '#027588',
      backgroundColor: 'white',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    cssAlamat: {
      borderWidth: 0.5,
      borderColor: '#027588',
      backgroundColor: 'white',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    cssTombol: {
      color: 'black',
      textAlign: 'center',
      backgroundColor: '#1f303a',
      fontWeight: 'bold',
      alignItem: 'center',
      fontSize: 25,
      width: 200,
      height: 70,
      borderRadius: 45,
      marginTop: 30,
      marginLeft: 90,
    },
  },
});

export default ProfileInput;
